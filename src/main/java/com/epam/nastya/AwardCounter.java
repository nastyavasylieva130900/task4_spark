package com.epam.nastya;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;


public class AwardCounter {

    public static void main(String[] args) throws Exception {

        SparkSession spark = SparkSession
                .builder()
                .master("local")
                .appName("AwardCounter")
                .getOrCreate();

        JavaRDD<String> players = spark.read().textFile("src/main/resources/Master.csv")
                .javaRDD();
        JavaRDD<String> awards = spark.read().textFile("src/main/resources/AwardsPlayers.csv")
                .javaRDD();

        JavaRDD<String> playersId = awards.map(s -> s.split(",")[0]);

        JavaPairRDD<String, Integer> idAndAward = playersId.mapToPair(s -> new Tuple2<>(s,1));
        JavaPairRDD<String, Integer> counts = idAndAward.reduceByKey((i1, i2) -> i1 + i2);

        JavaPairRDD<String, String> idAndFullName = players.mapToPair(s -> {
            String id = s.split(",")[0];
            String fullName = s.split(",")[3]
                    + " " + s.split(",")[4];
            return new Tuple2<>(id,fullName);
        });

        JavaPairRDD<String, Tuple2<String,Integer>> fullRDD = idAndFullName.join(counts);

        //List<Tuple2<String, Tuple2<String, Integer>>> output = fullRDD.collect();

        JavaRDD<String> result = fullRDD.map((s) -> s._2._1 + " - " + s._2()._2);
        result.saveAsTextFile("result.csv");

        spark.stop();

    }
}

